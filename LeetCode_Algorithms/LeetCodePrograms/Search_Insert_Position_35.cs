﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodePrograms
{
    public class Search_Insert_Position_35
    {
        public int SearchInsert(int[] nums, int target)
        {
            int index = 0;
            for(int i=0; i<nums.Length; i++)
            {
                if (target > nums[i])
                    index = i + 1;
                else
                    break;
            }
            return index;
        }
    }
}
