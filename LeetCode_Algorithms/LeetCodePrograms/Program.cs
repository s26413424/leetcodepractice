﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodePrograms
{
    class Program
    {
        static void Main(string[] args)
        {
            Find_First_and_Last_Position_34 model = new Find_First_and_Last_Position_34();
            var result = model.SearchRange2(new int[] { 0, 0, 0, 0, 0, 1, 2, 2, 3, 4, 4, 4, 4, 5, 5, 5, 6 }, 6);
            result = model.SearchRange2(new int[] { 5, 7, 7, 8, 8, 10 }, 6);
            result = model.SearchRange2(new int[] { 1, 3 }, 1);
            Console.ReadLine();
        }
    }
}
