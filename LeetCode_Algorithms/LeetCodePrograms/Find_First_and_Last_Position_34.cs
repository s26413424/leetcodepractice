﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodePrograms
{
    public class Find_First_and_Last_Position_34
    {
        public int[] SearchRange(int[] nums, int target)
        {
            int start = -1;
            int end = -1;
            bool isStart = false;
            for (int i = 0; i < nums.Length; i++)
            {
                var val = nums[i];
                if (val > target)
                    break;
                if (val == target)
                {
                    if (!isStart)
                    {
                        start = i;
                        end = i;
                        isStart = true;
                    }
                    else
                    {
                        end = i;
                    }
                }
            }
            return new int[2] { start, end };
        }

        public int[] SearchRange2(int[] nums, int target)
        {
            int len = nums.Length;
            int start = -1;
            int end = -1;
            var index = SearchIndex(nums, target, 0, nums.Length -1);
            if(index != -1)
            {
                start = index;
                end = index;
                for(int i = index - 1; i >= 0; i--)
                {
                    var val = nums[i];
                    if (val == target)
                    {
                        start = i;
                    }
                    else
                        break;
                }

                for(int i = index +1; i<len; i++)
                {
                    var val = nums[i];
                    if (val == target)
                    {
                        end = i;
                    }
                    else
                        break;
                }
            }

            return new int[2] { start, end };
        }

        public int SearchIndex(int[] nums, int target)
        {
            int Max = nums.Length;
            int Min = 0;
            int MaxCount = (nums.Length + 1) / 2;
            int i = (Min + Max) / 2;
            int result = -1;
            int count = 0;
            if (nums.Length > 0)
            {
                while (result != i && Min != Max && count <= MaxCount)
                {
                    var val = nums[i];
                    if (val == target)
                    {
                        result = i;
                    }
                    else if (val > target)
                    {
                        Max = i + 1;
                        i = (i + Min) / 2;
                    }
                    else
                    {
                        Min = i;
                        i = (i + Max) / 2;
                    }
                    count++;
                }
            }
            return result;
        }

        public int SearchIndex(int[] nums, int target, int start, int end)
        {
            if(start <= end)
            {
                int mid = (start + end) / 2;
                var val = nums[mid];
                if (target == nums[mid])
                    return mid;
                if(val < target)
                {
                    return SearchIndex(nums, target, mid + 1, end);
                }
                else
                {
                    return SearchIndex(nums, target, start, mid - 1);
                }
            }

            return -1;
        }
    }
}
