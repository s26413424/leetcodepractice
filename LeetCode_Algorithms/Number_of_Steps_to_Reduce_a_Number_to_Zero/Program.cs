﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Number_of_Steps_to_Reduce_a_Number_to_Zero
{
    class Program
    {
        /*Given a non-negative integer num, return the number of steps to reduce it to zero. If the current number is even, you have to divide it by 2, otherwise, you have to subtract 1 from it.*/
        static void Main(string[] args)
        {
            Console.WriteLine(NumberOfSteps(8));
            Console.ReadLine();
        }

        public static int NumberOfSteps(int num)
        {
            int step = 0;
            while(num != 0)
            {
                step++;
                num = num % 2 == 0 ? num / 2 : num - 1;
            }
            return step;
        }
    }
}
