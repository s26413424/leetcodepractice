﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Letter_Combinations_of_a_Phone_Number_17
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = LetterCombinations2("");

            Console.ReadLine();
        }

        public static IList<string> LetterCombinations(string digits)
        {
            List<string> result = new List<string>();

            Node root = new Node();
            Node ptr = root;
            for (int i = 0; i < digits.Length; i++)
            {
                ptr.MyCharList = GetPossibleCharsByNum(digits[i]);
                if (i != digits.Length - 1)
                {
                    ptr.Next = new Node();
                    ptr = ptr.Next;
                }

            }

            result = root.GetAllStr("");

            return result;
        }

        public static IList<string> LetterCombinations2(string digits)
        {
            List<string> answer = new List<string>();
            solve(digits, 0, string.Empty, answer);
            return answer;
        }

        public static void solve(string digits, int start, string temp, List<string> answer)
        {
            if(digits.Length == 0)
            {
                if(!string.IsNullOrEmpty(temp))
                    answer.Add(temp);
                return;
            }

            var ary = GetPossibleCharsByNum(digits[0]);
            start++;
            foreach (char ch in ary)
            {
                solve(digits.Substring(1), start, temp + ch, answer);
            }
        }

        public static char[] GetPossibleCharsByNum(char a)
        {
            switch (a)
            {
                case '2':
                    return new char[] { 'a', 'b', 'c' };
                case '3':
                    return new char[] { 'd', 'e', 'f' };
                case '4':
                    return new char[] { 'g', 'h', 'i' };
                case '5':
                    return new char[] { 'j', 'k', 'l' };
                case '6':
                    return new char[] { 'm', 'n', 'o' };
                case '7':
                    return new char[] { 'p', 'q', 'r', 's' };
                case '8':
                    return new char[] { 't', 'u', 'v' };
                case '9':
                    return new char[] { 'w', 'x', 'y', 'z' };
                default:
                    throw new Exception("Incorrect");
            }
        }

        public class Node
        {
            public char[] MyCharList { get; set; }

            public Node Next { get; set; }

            public List<string> GetAllStr(string str)
            {
                if (Next != null)
                {
                    List<string> result = new List<string>();
                    foreach (char ch in MyCharList)
                    {
                        result.AddRange(Next.GetAllStr(str + ch));
                    }

                    return result;
                }
                else if (MyCharList == null)
                {
                    return new List<string>();
                }
                else
                {
                    return MyCharList.Select(x => str + x.ToString()).ToList();
                }
            }
        }
    }
}
