﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Implement_Trie_208
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] input1 = new string[] { "insert", "search", "search", "startsWith", "startsWith", "insert", "search", "startsWith", "insert", "search", "startsWith" };
            string[] input2 = new string[] { "ab", "abc", "ab", "abc", "ab", "ab", "abc", "abc", "abc", "abc", "abc" };
            Trie trie = new Trie();
            int i = 0;
            foreach(string input in input1)
            {
                switch (input)
                {
                    case "insert":
                        trie.Insert(input2[i++]);
                        break;
                    case "search":
                        trie.Search(input2[i++]);
                        break;
                    case "startsWith":
                        trie.StartsWith(input2[i++]);
                        break;
                }
            }
            Console.ReadLine();
        }
    }

    public class Trie
    {
        /** Initialize your data structure here. */
        TrieContext tree;
        public Trie()
        {
            tree = new TrieContext();
        }

        /** Inserts a word into the trie. */
        public void Insert(string word)
        {
            if(!Search(word))
            {
                tree.Insert(word);
            }
        }

        /** Returns if the word is in the trie. */
        public bool Search(string word)
        {
            return tree.Search(word);
        }

        /** Returns if there is any word in the trie that starts with the given prefix. */
        public bool StartsWith(string prefix)
        {
            return tree.StartsWith(prefix);
        }

        /**
         * Your Trie object will be instantiated and called as such:
         * Trie obj = new Trie();
         * obj.Insert(word);
         * bool param_2 = obj.Search(word);
         * bool param_3 = obj.StartsWith(prefix);
         */

        internal class TrieContext
        {
            public string word { get; set; }

            public TrieContext left { get; set; }

            public TrieContext right { get; set; }

            public void Insert(string word)
            {
                if(this.word == null)
                {
                    this.word = word;
                    this.left = new TrieContext();
                    this.right = new TrieContext();
                    return;
                }

                if(string.Compare(this.word, word) > 0)
                {
                    this.right.Insert(word);
                }
                else
                {
                    this.left.Insert(word);
                }
            }

            public bool Search(string word)
            {
                if (this.word == null)
                    return false;
                int compareResult = string.Compare(this.word, word);
                if(compareResult == 0)
                {
                    return true;
                }
                else if(compareResult > 0 && this.right != null)
                {
                    return this.right.Search(word);
                }
                else if(compareResult < 0 && this.left != null)
                {
                    return this.left.Search(word);
                }
                else
                {
                    return false;
                }
            }

            public bool StartsWith(string prefix)
            {
                if (this.word == null)
                    return false;
                bool prefixResult = this.word.StartsWith(prefix);
                int compareResult = string.Compare(this.word, prefix);
                if (prefixResult)
                {
                    return true;
                }
                else if (compareResult > 0 && this.right != null)
                {
                    return this.right.StartsWith(prefix);
                }
                else if (compareResult < 0 && this.left != null)
                {
                    return this.left.StartsWith(prefix);
                }
                else
                {
                    return false;
                }
            }
        }
    }

}
