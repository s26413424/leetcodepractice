﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jewels_and_Stones_771
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        public int NumJewelsInStones(string J, string S)
        {
            return S.Count(x => J.Contains(x));
        }

        public int NumJewelsInStones2(string J, string S)
        {
            int count = 0;
            foreach(char s in S)
            {
                if (J.Contains(s))
                    count++;
            }
            return count;
        }
    }
}
